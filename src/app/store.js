import { createStore, combineReducers } from "redux";
import taskEvent from '../component/task/taskEvent';

const appReducer = combineReducers({
    taskReducer: taskEvent
});

const store = createStore(
    appReducer,
);

export default store;